CQPCSharp
===
#前言

通过酷Q平台的VC的SDK，来调用C#的类库。方便使用C#，和利用C#的庞大资源，来开发酷Q插件。

#项目说明
开发环境为vs2015，.net 版本默认为4.5.2。
##VC项目
###com.warpper.csharp
VC（CLR）封装，用来调用C#的类库。把此类库，放入app文件夹中。主是SDK的实现，和各种消息的中转。主要的实现会CsCQ.Core中。

##C#项目
C#项目生成的类库都放在酷Q根目录下的cs文件中。<br>
cs文件夹中的CS类库，除CsCQ.Core，切依赖于CsCQ.Core，实现可实时替换，无须重启酷Q，方便开发调试。
###CsCQ.Core
整个项目的核心功能，包括C#类库的加载，接口定义，和一部供VC使用的工具类.<br>
所有的C#项目都要引用该类库。
####接口定义
    ICqSdk                  SDK接口，VC实现，做为操作CQ的SDK包装，不用用户来实现，用户使用CqSdk.Default来使用SDK。
    IStartup                启动接口，插件启动时，反射所有继承接口类库，调用OnStartup方法。
    IGroupMessager          群消息接口，接收到群消息时，反射所有继承接口类库，调用OnGroupMessage方法。
    IPrivateMessager        私聊消息接口，接收私聊消息时，反射所有继承接口类库，调用OnPrivateMessage方法。
    IDiscussMessager        讨论组消息接口，接收讨论组消息时，反射所有继承接口类库，调用OnDiscussMessage方法。
	其它接口正在开发中...
####CsCQ.Demo
一个很简单的例子，现在是根本什么都没有。
####调试
附加酷Q进程，进行类库调试。..............别忘记pdb文件。
####注意
当前阶段所有实现IGroupMessager，IStartup，都是反射实例，且每次调用都是独立的实例，之后会加入缓存单例的设置。


