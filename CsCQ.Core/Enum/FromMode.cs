﻿namespace CsCQ_Core.Enum
{
    public enum FromMode
    {
        //subType 子类型，11/来自好友 1/来自在线状态 2/来自群 3/来自讨论组
        /// <summary>
        /// 来自在线状态
        /// </summary>
        Online = 1,
        /// <summary>
        /// 来自群
        /// </summary>
        Group = 2,
        /// <summary>
        /// 来自讨论组
        /// </summary>
        Discuss = 3,
        /// <summary>
        /// 来自好友
        /// </summary>
        Friend = 11,
    }
}
