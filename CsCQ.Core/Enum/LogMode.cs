﻿namespace CsCQ_Core.Enum
{
    public enum LogMode
    {
        Debug = 0,
        Info = 10,
        InfoSuccess = 11,//信息(成功) 紫色
        /// <summary>
        /// 信息(接收) 蓝色
        /// </summary>
        InfoRecv = 12,
        /// <summary>
        /// 信息(发送) 绿色
        /// </summary>
        InfoSend = 13,
        /// <summary>
        /// 警告 橙色
        /// </summary>
        Warning = 20,
        /// <summary>
        /// 错误 红色
        /// </summary>
        Error = 30,
        /// <summary>
        /// 致命错误 深红 
        /// </summary>
        Fatal = 40,
    }
}