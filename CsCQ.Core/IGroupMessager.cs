﻿
namespace CsCQ_Core
{
    public interface IGroupMessager
    {
        int OnGroupMessage(long sendTime, long groupId, long fromId, string anonymous, string message, int font);
    }
}