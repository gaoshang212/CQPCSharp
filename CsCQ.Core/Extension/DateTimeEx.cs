﻿using System;

namespace CsCQ_Core.Extension
{
    public static class DateTimeEx
    {
        public static long GetTimestamp(this DateTime p_date)
        {
            return (long)(p_date.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds;
        }

        public static DateTime GetDateTime(this long p_timestamp)
        {
            var ts = TimeSpan.FromMilliseconds(p_timestamp);

            return new DateTime(1970, 1, 1, 0, 0, 0, 0).Add(ts);
        }
    }
}