﻿using System;
using System.IO;

namespace CsCQ_Core.Extension
{
    public class PathEx
    {
        public static string Normal(string p_path)
        {
            var bd = AppDomain.CurrentDomain.BaseDirectory;

            return Path.Combine(bd, p_path);
        }
    }
}