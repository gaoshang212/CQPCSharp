﻿using CsCQ_Core.Enum;

namespace CsCQ_Core
{
    public interface IPrivateMessager
    {
        int OnPrivateMessage(FromMode p_mode, long sendTime, long fromId, string message, int font);
    }
}