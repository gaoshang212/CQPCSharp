﻿namespace CsCQ_Core
{
    public interface IDiscussMessager
    {
        int OnDiscussMessage(int sendTime, long fromDiscuss, long fromQQ, string p_message, int font);
    }
}