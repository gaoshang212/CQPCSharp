﻿using CsCQ_Core.Enum;

namespace CsCQ_Core
{
    public interface ICqSdk
    {
        /// <summary>
        /// 发送QQ私聊消息
        /// </summary>
        /// <param name="p_id">QQ号</param>
        /// <param name="p_message">消息</param>
        /// <returns></returns>
        int SendMessage(long p_id, string p_message);
        /// <summary>
        /// 发送群消息
        /// </summary>
        /// <param name="p_groupid">群号</param>
        /// <param name="p_message">消息</param>
        /// <returns></returns>
        int SendGroupMessage(long p_groupid, string p_message);
        /// <summary>
        /// 发送讨论组消息
        /// </summary>
        /// <param name="p_groupid">讨论组号</param>
        /// <param name="p_message">消息</param>
        /// <returns></returns>
        int SendDiscussMessage(long p_groupid, string p_message);
        /// <summary>
        /// 点赞
        /// </summary>
        /// <param name="id">QQ号</param>
        /// <returns></returns>
        int SendLike(long id);
        /// <summary>
        /// 群踢人
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="id">QQ号</param>
        /// <param name="rejectaddrequest"></param>
        /// <returns></returns>
        int SetGroupKick(long groupid, long id, bool rejectaddrequest);
        /// <summary>
        /// 禁止发言
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="id">QQ号</param>
        /// <param name="duration">时长</param>
        /// <returns></returns>
        int SetGroupBan(long groupid, long id, long duration);
        /// <summary>
        /// 设置管理员
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="id">QQ号</param>
        /// <param name="setadmin">设置/取消</param>
        /// <returns></returns>
        int SetGroupAdmin(long groupid, long id, bool setadmin);
        /// <summary>
        /// 全群禁言
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="enableban">设置/取消</param>
        /// <returns></returns>
        int SetGroupWholeBan(long groupid, bool enableban);
        /// <summary>
        /// 设置匿名群禁言
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="anomymous">匿名名字</param>
        /// <param name="duration">时长</param>
        /// <returns></returns>
        int SetGroupAnonymousBan(long groupid, string anomymous, long duration);
        /// <summary>
        /// 群匿名设置
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="enableanomymous">设置/取消</param>
        /// <returns></returns>
        int SetGroupAnonymous(long groupid, bool enableanomymous);
        /// <summary>
        /// 置群成员名片
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="id">QQ号</param>
        /// <param name="newcard">名片</param>
        /// <returns></returns>
        int SetGroupCard(long groupid, long id, string newcard);
        /// <summary>
        /// 群退出
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="isdismiss">解散</param>
        /// <returns></returns>
        int SetGroupLeave(long groupid, bool isdismiss);
        /// <summary>
        /// 设置群成员专属头衔
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="id">QQ号</param>
        /// <param name="newspecialtitle">头衔</param>
        /// <param name="duration">时长</param>
        /// <returns></returns>
        int SetGroupSpecialTitle(long groupid, long id, string newspecialtitle, long duration);
        /// <summary>
        /// 讨论组退出
        /// </summary>
        /// <param name="discussid">讨论组号</param>
        /// <returns></returns>
        int SetDiscussLeave(long discussid);
        /// <summary>
        /// 添加好友请求
        /// </summary>
        /// <param name="responseflag"></param>
        /// <param name="responseoperation"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        int SetFriendAddRequest(string responseflag, int responseoperation, string remark);
        /// <summary>
        /// 添加群请求
        /// </summary>
        /// <param name="responseflag"></param>
        /// <param name="requesttype"></param>
        /// <param name="responseoperation"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        int SetGroupAddRequestV2(string responseflag, RequstMode requesttype, int responseoperation, string reason);
        /// <summary>
        /// 获取群成员信息
        /// </summary>
        /// <param name="groupid">群号</param>
        /// <param name="id">QQ号</param>
        /// <param name="nocache"></param>
        /// <returns></returns>
        string GetGroupMemberInfoV2(long groupid, long id, bool nocache);
        /// <summary>
        /// 获取陌生人信息
        /// </summary>
        /// <param name="id">QQ号</param>
        /// <param name="nocache"></param>
        /// <returns></returns>
        string GetStrangerInfo(long id, bool nocache);
        /// <summary>
        /// 获取Cookies
        /// </summary>
        /// <returns></returns>
        string GetCookies();
        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        int GetCsrfToken();
        /// <summary>
        /// 获取登录QQ号
        /// </summary>
        /// <returns></returns>
        long GetLoginQQ();
        /// <summary>
        /// 获取登录QQ名称
        /// </summary>
        /// <returns></returns>
        string GetLoginNick();
        /// <summary>
        /// 获取应用路径
        /// </summary>
        /// <returns></returns>
        string GetAppDirectory();
        /// <summary>
        /// 输出日志
        /// </summary>
        /// <param name="p_mode"></param>
        /// <param name="p_title"></param>
        /// <param name="p_message"></param>
        /// <returns></returns>
        int Log(LogMode p_mode, string p_title, string p_message);
        /// <summary>
        /// 崩溃
        /// </summary>
        /// <param name="errorinfo">错误日志</param>
        /// <returns></returns>
        int Fatal(string errorinfo);
    }
}