﻿using CsCQ_Core.Enum;

namespace CsCQ_Core
{
    public class CqSdk
    {
        private static ICqSdk _cqSdk;

        public static ICqSdk Default
        {
            get { return _cqSdk; }
        }

        private CqSdk()
        {

        }

        /// <summary>
        /// 设置SDK对象
        /// </summary>
        /// <param name="p_cqSdk"></param>
        internal static void SetSdk(ICqSdk p_cqSdk)
        {
            _cqSdk = p_cqSdk;

            if (_cqSdk != null)
            {
                _cqSdk.Log(LogMode.Debug, "C#", "set sdk sucss");
            }
        }

        public static string GetAt(long id)
        {
            return $"[CQ:at,qq={id}]";
        }

        public static string GetShare(string p_url, string p_title, string p_content, string p_image = "")
        {
            return $"[CQ:share,url={p_url},title={p_title},content={p_content},image={p_image}] ";
        }
    }
}