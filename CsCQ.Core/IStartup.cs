﻿namespace CsCQ_Core
{
    public interface IStartup
    {
        void OnStartup();
    }
}