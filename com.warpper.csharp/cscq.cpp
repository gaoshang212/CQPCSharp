#include "stdafx.h"
#include "cscq.h"

CsCQ::CsCQ(int ac)
{
	System::AppDomain::CurrentDomain->AssemblyResolve += gcnew System::ResolveEventHandler(&OnAssemblyResolve);
	//LoadAssemblys();
	_ac = ac;

	Init();
}

void CsCQ::Init()
{
	auto sdk = gcnew CqSdk(_ac);
	CqSdk::Default = sdk;

	CsCQ_Core::CqHelper::LoadLibs();
	CsCQ_Core::CqHelper::SetSdk(sdk);
}

int CsCQ::receivePrivateMsg(int32_t subtype, int32_t sendTime, int64_t fromQQ, const char *msg, int32_t font)
{
	auto sdk = CqSdk::Default;
	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "PrivateMsg Start");

	array<CsCQ_Core::IPrivateMessager^>^ instances = CsCQ_Core::CqHelper::GetPrivateMessages();

	if (instances->Length <= 0)
	{
		sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "IPrivateMessage is empty.");
		return EVENT_IGNORE;
	}

	auto ref = 0;

	auto message = gcnew System::String(msg);

	for each (auto instance in instances)
	{
		ref = instance->OnPrivateMessage((CsCQ_Core::Enum::FromMode)subtype, sendTime, fromQQ, message, font);
	}

	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "PrivateMsg End");

	return ref;
}


int CsCQ::receiveGroupMsg(int32_t sendTime, int64_t fromGroup, int64_t fromQQ, const char *fromAnonymous, const char *msg, int32_t font)
{
	auto sdk = CqSdk::Default;
	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "Start");

	array<CsCQ_Core::IGroupMessager^>^ instances = CsCQ_Core::CqHelper::GetMessagers();

	if (instances->Length <= 0)
	{
		sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "instances = 0");
		return EVENT_IGNORE;
	}

	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "instances > 0");

	auto ref = 0;

	auto fam = gcnew System::String(fromAnonymous);
	auto message = gcnew System::String(msg);

	for each (auto instance in instances)
	{
		ref = instance->OnGroupMessage(sendTime, fromGroup, fromQQ, fam, message, font);
	}

	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "End");

	return ref;
}

int CsCQ::receiveDiscussMsg(int32_t sendTime, int64_t fromDiscuss, int64_t fromQQ, const char *msg, int32_t font)
{
	auto sdk = CqSdk::Default;
	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "DiscussMsg Start");

	auto instances = CsCQ_Core::CqHelper::GetDiscussMessager();

	if (instances->Length <= 0)
	{
		sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "IDiscussMessager is empty.");
		return EVENT_IGNORE;
	}

	auto ref = 0;

	auto message = gcnew System::String(msg);

	for each (auto instance in instances)
	{
		ref = instance->OnDiscussMessage(sendTime, fromDiscuss, fromQQ, message, font);
	}

	sdk->Log(CsCQ_Core::Enum::LogMode::Debug, "CSCQ", "DiscussMsg End");

	return ref;
}

void CsCQ::startup()
{
	auto startups = CsCQ_Core::CqHelper::GetStartups();

	for each (auto startup in startups)
	{
		startup->OnStartup();
	}
}

System::Reflection::Assembly ^ CsCQ::OnAssemblyResolve(System::Object ^sender, System::ResolveEventArgs ^args)
{
	//auto buffer = System::IO::File::ReadAllBytes("./cs/CsCQ.Core.dll");
	return System::Reflection::Assembly::LoadFrom("./cs/CsCQ.Core.dll");
}
