#pragma once
#include "CqSdk.h"

using namespace System;

class CsCQ
{
public:
	CsCQ(int ac);

	void startup();

	int receivePrivateMsg(int32_t subtype, int32_t sendTime, int64_t fromQQ, const char *msg, int32_t font);
	int receiveGroupMsg(int32_t sendTime, int64_t fromGroup, int64_t fromQQ, const char *fromAnonymous, const char *msg, int32_t font);
	int receiveDiscussMsg(int32_t sendTime, int64_t fromDiscuss, int64_t fromQQ, const char *msg, int32_t font);

private:
	static System::Reflection::Assembly ^ OnAssemblyResolve(System::Object ^sender, System::ResolveEventArgs ^args);

	void Init();
	//void LoadAssemblys();
	bool _isLoad = false;

	int _ac = -1;

protected:

	/*String^ stringVar;*/
};






